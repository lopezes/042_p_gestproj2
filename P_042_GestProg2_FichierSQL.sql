-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 31 Octobre 2018 à 08:58
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_p_042_gestprog2`
--
CREATE DATABASE IF NOT EXISTS `db_p_042_gestprog2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_p_042_gestprog2`;

-- --------------------------------------------------------

--
-- Structure de la table `t_core`
--

CREATE TABLE `t_core` (
  `idCore` int(11) NOT NULL,
  `corNumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_core`
--

INSERT INTO `t_core` (`idCore`, `corNumber`) VALUES
(1, 8),
(2, 6),
(3, 4),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_mobile`
--

CREATE TABLE `t_mobile` (
  `idMobile` int(11) NOT NULL,
  `mobName` varchar(50) NOT NULL,
  `mobReleaseDate` date NOT NULL,
  `mobMark` varchar(50) NOT NULL,
  `mobProvider` varchar(50) NOT NULL,
  `mobPrice` float NOT NULL,
  `mobReleasePrice` float NOT NULL,
  `mobCoreModel` varchar(50) NOT NULL,
  `mobFrequency` float NOT NULL,
  `mobBatteryAutonomy` varchar(50) NOT NULL,
  `fkOS` int(11) DEFAULT NULL,
  `fkCore` int(11) DEFAULT NULL,
  `fkRAM` int(11) DEFAULT NULL,
  `fkScreen` int(11) DEFAULT NULL,
  `fkStorage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_mobile`
--

INSERT INTO `t_mobile` (`idMobile`, `mobName`, `mobReleaseDate`, `mobMark`, `mobProvider`, `mobPrice`, `mobReleasePrice`, `mobCoreModel`, `mobFrequency`, `mobBatteryAutonomy`, `fkOS`, `fkCore`, `fkRAM`, `fkScreen`, `fkStorage`) VALUES
(1, 'Moto Z Play', '2016-09-01', 'Lenovo', 'fnac.com', 289, 499, 'Qualcomm Adreno 506', 2, '18:00', 2, 1, 4, 7, 3),
(2, 'Moto G6', '2018-05-01', 'Lenovo', 'amazon.fr', 199.99, 249, 'Qualcomm Snapdragon 450', 1.8, '07:00', 5, 1, 4, 9, 2),
(3, 'Samsung Galaxy S9', '2018-03-01', 'Samsung', 'rakuten.com', 538.5, 859, 'Samsung Exynos 9810', 2.7, '11:00', 5, 1, 3, 10, 2),
(4, 'Samsung Galaxy  A6', '2018-06-01', 'Samsung', 'fnac.com', 189.99, 309, 'Exynos 7870', 1.6, '16:00', 5, 1, 4, 8, 3),
(5, 'Xiaomi Redmi Note 5A', '2018-08-01', 'Xiaomi', 'rakuten.com', 123, 159, 'Qualcomm Snapdragon MSM8917', 1.4, '09:00', 3, 3, 5, 7, 4),
(6, 'Xiaomi Redmi 6A', '2018-07-01', 'Xiaomi', 'fnac.com', 103.94, 119, 'Helio A22', 2, '07:00', 6, 1, 5, 7, 4),
(7, 'OnePlus 5T', '2017-11-01', 'OnePlus', 'rakuten.com', 435.89, 499, 'Qualcomm Snapdragon 835', 2.45, '12:00', 4, 1, 2, 11, 1),
(8, 'OnePlus 6', '2018-05-01', 'OnePlus', 'rakuten.com', 409, 519, 'Qualcomm Snapdragon 845', 2.8, '10:00', 6, 1, 2, 12, 2),
(9, 'Honor View 10', '2018-01-01', 'Huawei', 'pc-ostschweiz.ch', 446.9, 599.9, 'HiSilicon Kirin 970', 2.36, '14:00', 5, 1, 2, 11, 3),
(10, 'Huawei P20', '2018-03-01', 'Huawei', 'fust.ch', 449, 653, 'HiSilicon Kirin 970', 2.36, '05:00', 6, 1, 3, 10, 1),
(11, 'Nokia 5', '2017-08-01', 'Nokia', 'pc-ostschweiz.ch', 175.9, 219, 'Qualcomm Snapdragon 430', 1.4, '12:00', 4, 1, 5, 4, 4),
(12, 'Nokia 1', '2018-04-01', 'Nokia', 'microspot.ch', 86.1, 99, 'MediaTek MT6737M', 1.1, '09:00', 6, 1, 7, 2, 5),
(13, 'iPhone 7 Plus', '2016-09-01', 'Apple', 'microspot.ch', 669, 883, 'A10 Fusion', 2.34, '07:00', 7, 3, 5, 7, 3),
(14, 'iPhone X', '2017-11-01', 'Apple', 'pc-ostschweiz.ch', 1189.9, 1249, 'A11 Bionic', 2.39, '12:00', 7, 2, 4, 10, 2),
(15, 'Wiko Sunny 2', '2017-07-01', 'Wiko', 'mediamarkt.ch', 65.95, 59, 'Cortex-A7', 1.2, '03:00', 2, 3, 3, 1, 5),
(16, 'Wiko Jerry 3', '2018-05-01', 'Wiko', 'mobilezone.ch', 75.85, 79, 'MediaTek MT6580M', 1.3, '03:00', 5, 3, 7, 6, 4),
(17, 'Microsoft Lumia 650', '2016-02-01', 'Microsoft', 'pc-ostschweiz.ch', 179.9, 149.99, 'Qualcomm Snapdragon 212', 1.3, '07:00', 8, 3, 7, 3, 4),
(18, 'LG G6', '2017-04-01', 'LG', 'microspot.ch', 324, 749, 'Snapdragon 821', 2.15, '10:00', 3, 3, 3, 9, 3),
(19, 'LG K10', '2017-05-01', 'LG', 'mobilezero.ch', 162.2, 184.9, 'MediaTek MT6750', 1.5, '13:06', 3, 1, 5, 5, 4),
(20, 'Xperia XZ2', '2018-04-01', 'Sony', 'microspot.ch', 547, 679, 'Qualcomm Snapdragon 845', 2.7, '06:00', 5, 1, 3, 3, 2),
(21, 'Xperia Z5 Dual', '2015-09-01', 'Sony', 'handy4.ch', 299, 999, 'Qualcomm MSM8994 Snapdragon 810', 1.8, '09:00', 1, 1, 4, 4, 3),
(22, 'BlackBerry Key 2', '2018-06-01', 'blackberry', 'pc-ostschweiz.ch', 678.9, 721.35, 'Qualcomm Snapdragon 660', 2.2, '16:56', 6, 1, 2, 2, 2),
(23, 'BLACKBERRY DTEK50', '2016-09-01', 'blackberry', 'digitec.ch', 314, 294.35, 'Qualcomm Snapdragon 617', 1.5, '05:55', 2, 1, 4, 4, 4),
(24, 'Alcatel 5 ', '2018-03-01', 'Alcatel', 'pc-ostschweiz.ch', 235.9, 243.65, 'MediaTek MT6750 ', 1.5, '18:00', 4, 1, 4, 9, 3),
(25, 'Alcatel A3', '2017-06-01', 'Alcatel', 'digitec.ch', 127, 143.65, 'MediaTek MT6737', 1.25, '10:00', 2, 3, 6, 3, 4),
(26, 'ZTE Blade V8', '2017-07-01', 'ZTE', 'pc-ostschweiz.ch', 175.9, 243.85, 'Qualcomm Snapdragon 435  ', 1.4, '10:00', 3, 1, 4, 4, 3),
(27, 'HTC Desire 12+', '2018-07-01', 'HTC', 'mediamarkt.ch', 205.95, 229, 'Qualcomm Snapdragon 450', 1.8, '06:00', 5, 1, 2, 11, 3),
(28, 'MICROSOFT Lumia 650', '2016-02-01', 'Microsoft', 'interdiscount.ch', 199, 214.15, 'Qualcomm Snapdragon 212', 1.3, '07:00', 8, 3, 7, 3, 4),
(29, 'Razer Phone', '2017-11-01', 'Razer', 'digitec.ch', 724, 849, 'Qualcomm Snapdragon 835', 2.45, '10:24', 3, 1, 1, 9, 2),
(30, ' ZTE Blade L110', '2016-04-01', 'ZTE', 'pc-ostschweiz.ch', 75.85, 62.2, 'Spreadtrum SC7731 ', 1.2, '02:00', 1, 4, 8, 1, 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_os`
--

CREATE TABLE `t_os` (
  `idOS` int(11) NOT NULL,
  `osName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_os`
--

INSERT INTO `t_os` (`idOS`, `osName`) VALUES
(1, 'Android 5.1 Lollipop'),
(2, 'Android 6.0 Marshmallow'),
(3, 'Android 7.0 Nougat'),
(4, 'Android 7.1 Nougat'),
(5, 'Android 8.0 Oreo'),
(6, 'Android 8.1 Oreo'),
(7, 'iOS'),
(8, 'Windows 10 Mobile  ');

-- --------------------------------------------------------

--
-- Structure de la table `t_price`
--

CREATE TABLE `t_price` (
  `idPrice` int(11) NOT NULL,
  `priDate` date  NOT NULL,
  `priQuantity` float(11) NOT NULL,
  `fkMobile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_price`(`idPrice`,`priDate`, `priQuantity`,`fkMobile`)VALUES
(1, '2016-11-08',429.00, 1 ),
(2, '2017-01-05',414.00, 1 ),
(3, '2017-03-10',371.00, 1 ),
(4, '2017-05-08',378.95, 1 ),
(5, '2017-07-29',319.95, 1 ),
(6, '2017-11-11',364.30, 1 ),
(7, '2018-01-03',341.00, 1 ),
(8, '2018-03-06',330.50, 1 ),
(9, '2018-05-25',337.70, 1 ),
(10, '2018-07-07',293.00, 1 ),
(11, '2018-09-07',234.80, 1 ),
(12, '2018-11-07',528.45, 1 ),
(13, '2018-07-21',259.90, 2 ),
(14, '2018-09-08',246.00, 2 ),
(15, '2018-11-07',232.90, 2 ),
(16, '2018-04-26',749.00, 3 ),
(17, '2018-06-26',643.85, 3 ),
(18, '2018-08-30',613.90, 3 ),
(19, '2018-10-30',600.90, 3 ),
(20, '2018-11-07',232.90, 3 ),
(21, '2018-11-14',579.00, 3 ),
(22, '2018-08-01',248.90, 4 ),
(23, '2018-10-02',248.85, 4 ),
(24, '2018-11-14',236.90, 4 ),
(25, '2017-11-07',146.00, 5 ),
(26, '2018-01-12',135.00, 5 ),
(27, '2018-03-10',122.30, 5 ),
(28, '2018-15-11',119.20, 5 ),
(29, '2018-07-10',119.90, 5 ),
(30, '2018-09-03',121.00, 5 ),
(31, '2018-11-14',119.00, 5 ),
(32, '2018-09-03',118.00, 6 ),
(33, '2018-11-03',119.00, 6 ),
(34, '2018-11-14',116.90, 6 ),
(35, '2018-01-02',598.00, 7 ),
(36, '2018-03-04',568.90, 7 ),
(37, '2018-05-10',564.95, 7 ),
(38, '2018-07-07',542.55, 7 ),
(39, '2018-09-03',517.00, 7 ),
(40, '2018-11-14',509.00, 7 ),
(41, '2018-07-09',628.85, 8 ),
(42, '2018-09-08',565.75, 8 ),
(43, '2018-11-14',566.00, 8 ),
(44, '2018-03-08',484.05, 9 ),
(45, '2018-05-08',513.00, 9 ),
(46, '2018-07-07',486.80, 9 ),
(47, '2018-09-07',446.30, 9 ),
(48, '2018-11-14',424.00, 9 ),
(49, '2018-05-02',630.90, 10 ),
(50, '2018-07-02',588.05, 10 ),
(51, '2018-09-02',404.10, 10 ),
(52, '2018-11-14',476.00, 10 ),
(53, '2017-10-08',189.15, 11 ),
(54, '2017-12-08',176.95, 11 ),
(55, '2018-02-03',194.85, 11 ),
(56, '2018-04-05',179.00, 11 ),
(57, '2018-06-05',178.85, 11 ),
(58, '2018-08-03',173.45, 11 ),
(59, '2018-10-05',175.90, 11 ),
(60, '2018-11-14',175.00, 11 ),
(61, '2018-07-26',79.10, 12 ),
(62, '2018-08-15',86.05, 12 ),
(63, '2016-11-10',881.95, 13 ),
(64, '2017-01-09',885.00, 13 ),
(65, '2017-03-10',846.00, 13 ),
(66, '2017-05-15',748.00, 13 ),
(67, '2017-07-15',769.00, 13 ),
(68, '2017-09-15',755.15, 13 ),
(69, '2017-11-14',735.00, 13 ),
(70, '2018-01-10',743.30, 13 ),
(71, '2018-03-09',757.85, 13 ),
(72, '2018-05-10',740.00, 13 ),
(73, '2018-07-10',669.00, 13 ),
(74, '2018-09-13',661.00, 13 ),
(75, '2018-11-14',661.00, 13 ),
(76, '2018-01-10',1162.00, 14 ),
(77, '2018-03-06',1066.90, 14 ),
(78, '2018-05-07',1073.00, 14 ),
(79, '2018-07-11',1079.00, 14 ),
(80, '2018-09-12',995.00, 14 ),
(81, '2018-11-14',943.60, 14 ),
(82, '2017-09-06',66.90, 15 ),
(83, '2017-11-02',65.90, 15 ),
(84, '2018-01-01',64.00, 15 ),
(85, '2018-03-20',66.60, 15 ),
(86, '2018-05-08',62.75, 15 ),
(87, '2018-07-07',62.75, 15 ),
(88, '2018-09-14',67.00, 15 ),
(89, '2018-11-14',59.95, 15 ),
(90, '2018-07-05',77.00, 16 ),
(91, '2018-09-03',75.80, 16 ),
(92, '2018-11-14',71.85, 16 ),
(93, '2016-04-06',195.55, 17 ),
(94, '2016-06-08',194.10, 17 ),
(95, '2016-08-02',138.00, 17 ),
(96, '2016-10-05',120.70, 17 ),
(97, '2016-12-06',122.30, 17 ),
(98, '2017-02-27',133.00, 17 ),
(99, '2017-04-04',134.00, 17 ),
(100, '2017-06-01',138.00, 17 ),
(101, '2017-07-06',199.00, 17 ),
(102, '2017-12-25',599.00, 17 ),
(103, '2018-02-13',599.00, 17 ),
(104, '2018-04-27',288.00, 17 ),
(105, '2018-06-06',246.00, 17 ),
(106, '2018-09-06',222.00, 17 ),
(107, '2018-11-14',197.00, 17 ),
(108, '2017-07-25',566.00, 18 ),
(109, '2017-10-10',529.00, 18 ),
(110, '2017-12-07',649.00, 18 ),
(111, '2018-02-10',579.00, 18 ),
(112, '2018-06-08',549.00, 18 ),
(113, '2018-08-05',519.00, 18 ),
(114, '2018-11-14',499.00, 18 ),
(115, '2017-08-03',184.90, 19 ),
(116, '2017-12-21',175.45, 19 ),
(117, '2018-03-15',175.00, 19 ),
(118, '2018-03-15',175.00, 19 ),
(119, '2018-05-14',149.00, 19 ),
(120, '2018-07-11',157.65, 19 ),
(121, '2018-09-12',160.00, 19 ),
(122, '2018-11-14',162.10, 19 ),
(123, '2018-06-05',593.90, 20 ),
(124, '2018-08-09',583.85, 20 ),
(125, '2018-10-09',559.00, 20 ),
(126, '2018-11-14',544.00, 20 ),
(127, '2016-06-04',589.00, 21 ),
(128, '2016-11-08',489.00, 21 ),
(129, '2017-01-04',482.85, 21 ),
(130, '2017-03-08',468.00, 21 ),
(131, '2017-05-09',416.00, 21 ),
(132, '2017-07-07',398.95, 21 ),
(133, '2017-09-08',368.65, 21 ),
(134, '2017-11-08',349.00, 21 ),
(135, '2018-01-11',343.70, 21 ),
(136, '2018-03-11',342.35, 21 ),
(137, '2018-05-29',369.00, 21 ),
(138, '2018-07-31',297.00, 21 ),
(139, '2018-09-09',317.45, 21 ),
(140, '2018-11-14',299.00, 21 ),
(141, '2018-08-02',744.75, 22 ),
(142, '2018-10-02',694.30, 22 ),
(143, '2018-11-21',687.05, 22 ),
(144, '2016-11-02',295.00, 23 ),
(145, '2017-01-05',245.80, 23 ),
(146, '2017-03-23',241.65, 23 ),
(147, '2017-05-02',242.05, 23 ),
(148, '2017-07-02',252.75, 23 ),
(149, '2017-09-15',237.00, 23 ),
(150, '2017-12-18',236.00, 23 ),
(151, '2018-02-07',237.90, 23 ),
(152, '2018-04-11',238.00, 23 ),
(153, '2018-08-02',238.00, 23 ),
(154, '2018-11-21',266.00, 23 ),
(155, '2018-07-02',243.65, 24 ),
(156, '2018-09-01',207.90, 24 ),
(157, '2018-11-21',237.70, 24 ),
(158, '2017-08-02',150.80, 25 ),
(159, '2017-10-02',149.85, 25 ),
(160, '2017-12-02',164.90, 25 ),
(161, '2018-02-02',152.55, 25 ),
(162, '2018-04-07',143.35, 25 ),
(163, '2018-06-05',127.00, 25 ),
(164, '2018-07-07',113.00, 25 ),
(165, '2018-09-12',127.00, 25 ),
(166, '2018-11-21',127.00, 25 ),
(167, '2017-09-07',235.95, 26 ),
(168, '2017-11-02',239.15, 26 ),
(169, '2018-01-02',221.80, 26 ),
(170, '2018-03-13',213.90, 26 ),
(171, '2018-05-17',196.00, 26 ),
(172, '2018-07-06',194.00, 26 ),
(173, '2018-09-09',170.30, 26 ),
(174, '2018-11-21',165.00, 26 ),
(175, '2018-09-02',224.10, 27 ),
(176, '2018-11-21',199.00, 27 ),
(177, '2016-04-01',195.70, 28 ),
(178, '2016-06-08',194.10, 28 ),
(179, '2016-08-02',138.00, 28 ),
(180, '2016-10-05',120.70, 28 ),
(181, '2016-12-06',122.30, 28 ),
(182, '2017-02-27',133.00, 28 ),
(183, '2017-04-04',134.00, 28 ),
(184, '2017-06-01',138.00, 28 ),
(185, '2017-07-18',299.00, 28 ),
(186, '2017-12-25',599.00, 28 ),
(187, '2017-12-25',599.00, 28 ),
(188, '2018-02-13',599.00, 28 ),
(189, '2018-04-27',288.00, 28 ),
(190, '2018-06-06',246.00, 28 ),
(191, '2018-09-01',224.00, 28 ),
(192, '2018-11-21',200.40, 28 ),
(193, '2018-05-11',849.00, 29 ),
(194, '2018-07-09',799.00, 29 ),
(195, '2018-09-07',724.00, 29 ),
(196, '2018-11-21',799.00, 29 ),
(197, '2017-06-22',62.25, 30 ),
(198, '2017-08-02',69.85, 30 ),
(199, '2017-10-02',61.75, 30 ),
(200, '2017-12-07',73.45, 30 ),
(201, '2018-02-06',61.00, 30 ),
(202, '2018-04-03',74.55, 30 ),
(203, '2018-06-04',75.30, 30 ),
(204, '2018-08-03',75.35, 30 ),
(205, '2018-10-16',75.85, 30 ),
(206, '2018-11-21',75.85, 30 );
-- --------------------------------------------------------

--
-- Structure de la table `t_ram`
--

CREATE TABLE `t_ram` (
  `idRAM` int(11) NOT NULL,
  `ramSize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_ram`
--

INSERT INTO `t_ram` (`idRAM`, `ramSize`) VALUES
(1, 8),
(2, 6),
(3, 4),
(4, 3),
(5, 2),
(6, 2),
(7, 1),
(8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_screensize`
--

CREATE TABLE `t_screensize` (
  `idScreen` int(11) NOT NULL,
  `scrSize` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_screensize`
--

INSERT INTO `t_screensize` (`idScreen`, `scrSize`) VALUES
(1, 4),
(2, 4.5),
(3, 5),
(4, 5.2),
(5, 5.3),
(6, 5.4),
(7, 5.5),
(8, 5.6),
(9, 5.7),
(10, 5.8),
(11, 6),
(12, 6.28);

-- --------------------------------------------------------

--
-- Structure de la table `t_storage`
--

CREATE TABLE `t_storage` (
  `idStorage` int(11) NOT NULL,
  `stoSize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_storage`
--

INSERT INTO `t_storage` (`idStorage`, `stoSize`) VALUES
(1, 128),
(2, 64),
(3, 32),
(4, 16),
(5, 8),
(6, 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_core`
--
ALTER TABLE `t_core`
  ADD PRIMARY KEY (`idCore`);

--
-- Index pour la table `t_mobile`
--
ALTER TABLE `t_mobile`
  ADD PRIMARY KEY (`idMobile`),
  ADD KEY `FK_t_mobile_idOS` (`fkOS`),
  ADD KEY `FK_t_mobile_idCore` (`fkCore`),
  ADD KEY `FK_t_mobile_idRAM` (`fkRAM`),
  ADD KEY `FK_t_mobile_idScreen` (`fkScreen`),
  ADD KEY `FK_t_mobile_idStorage` (`fkStorage`);

--
-- Index pour la table `t_os`
--
ALTER TABLE `t_os`
  ADD PRIMARY KEY (`idOS`);

--
-- Index pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD PRIMARY KEY (`idPrice`),
  ADD KEY `FK_t_price_idMobile` (`fkMobile`);

--
-- Index pour la table `t_ram`
--
ALTER TABLE `t_ram`
  ADD PRIMARY KEY (`idRAM`);

--
-- Index pour la table `t_screensize`
--
ALTER TABLE `t_screensize`
  ADD PRIMARY KEY (`idScreen`);

--
-- Index pour la table `t_storage`
--
ALTER TABLE `t_storage`
  ADD PRIMARY KEY (`idStorage`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_core`
--
ALTER TABLE `t_core`
  MODIFY `idCore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_mobile`
--
ALTER TABLE `t_mobile`
  MODIFY `idMobile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_os`
--
ALTER TABLE `t_os`
  MODIFY `idOS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_price`
--
ALTER TABLE `t_price`
  MODIFY `idPrice` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_ram`
--
ALTER TABLE `t_ram`
  MODIFY `idRAM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_screensize`
--
ALTER TABLE `t_screensize`
  MODIFY `idScreen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_storage`
--
ALTER TABLE `t_storage`
  MODIFY `idStorage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_mobile`
--
ALTER TABLE `t_mobile`
  ADD CONSTRAINT `t_Mobile_t_CORE_FK` FOREIGN KEY (`fkCore`) REFERENCES `t_core` (`idCore`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_Mobile_t_OS_FK` FOREIGN KEY (`fkOS`) REFERENCES `t_os` (`idOS`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_Mobile_t_RAM_FK` FOREIGN KEY (`fkRAM`) REFERENCES `t_ram` (`idRAM`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_Mobile_t_ScreenSize_FK` FOREIGN KEY (`fkScreen`) REFERENCES `t_screensize` (`idScreen`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_Mobile_t_Storage1_FK` FOREIGN KEY (`fkStorage`) REFERENCES `t_storage` (`idStorage`) ON DELETE CASCADE;

--
-- Contraintes pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD CONSTRAINT `t_Price_t_Mobile_FK` FOREIGN KEY (`fkMobile`) REFERENCES `t_mobile` (`idMobile`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
